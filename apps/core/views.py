from django.shortcuts import render, get_object_or_404
from .models import Secretaria,Representante,Requisicao,Pregao,Beneficiaria,Item,EDespesa,ReqItem,Autorizacao,AutorizacaoReqItens
from .forms import RequisicaoForm, ReqItemForm, CsvForm
from .logic import ReportGenerator
from django.utils import formats
from django.http import HttpResponse, HttpResponseRedirect
from collections import defaultdict
from .extenso import converter_extenso
import csv, locale
locale.setlocale(locale.LC_ALL, '')

def index(request):
    template_name = 'index.html'
    rows = Requisicao.objects.all()
    context = dict(rows=rows)
    return render(request,template_name,context)


def beneficiarias(request):
    template_name = 'beneficiarias/index.html'
    rows = Beneficiaria.objects.all()
    context = dict(rows=rows)
    return render(request,template_name,context)


def secretarias(request):
    template_name = 'secretarias/index.html'
    rows = Secretaria.objects.all()
    context = dict(rows=rows)
    return render(request,template_name,context)


def beneficiaria_view(request,pk):
    template_name = 'beneficiarias/view.html'
    rows = get_object_or_404(Beneficiaria, pk=pk)
    context = dict(rows=rows)
    return render(request,template_name,context)


def requisicao_add(request):
    template_name = 'requisicao/add.html'
    form = RequisicaoForm(request.POST)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
    else:
        form = RequisicaoForm()
    context = dict(form=form)
    return render(request,template_name,context)


def requisicao_view(request,pk,pessoa):
    template_name = 'requisicao/view.html'
    requisicao = get_object_or_404(Requisicao,pk=pk)
    itens = ReqItem.objects.filter(fk_requisicao=pk).order_by('ed')
    erro = None
    eds = requisicao.ed.split(',')
    beneficiarias = Beneficiaria.objects.filter(
        CNPJ__in=itens.values_list('item__fk_beneficiaria', flat=True)
        )
    representante = Representante.objects.filter(
        cpf__in=itens.values_list('item__fk_representante', flat=True)
        )
    total = total_f(itens,i='id',q='quantidade',v='item__valor_unit')

    autorizacao = Autorizacao.objects.filter(fk_requisicao=pk)
    
    """Form Autorização """
    form = ReqItemForm(request.POST)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/requisicao/{}/{}/'.format(pessoa,pk))
        else:
            erro = "#myModal"
    else:
        form = ReqItemForm(initial={'fk_requisicao': pk,'ordem':itens.count()+1})
        form.fields['item'].queryset = Item.objects.filter(fk_pregao=requisicao.fk_pregao,)
        form.fields['ed'].queryset = EDespesa.objects.filter(ed__in=eds)

    context = dict(
        requisicao=requisicao,
        ano=requisicao.get_ano_display(),
        data_pro=formats.date_format(requisicao.data_pro, "SHORT_DATE_FORMAT"),
        data_ass=formats.date_format(requisicao.assinatura, "SHORT_DATE_FORMAT"),
        erro=erro,
        form=form,
        itens=zip(itens,total['subtotal']),
        total=','.join(str(total['total']).split('.')),
        autorizacao=autorizacao,
        eds = [mask(str(ed),'ed') for ed in eds],
        extenso = converter_extenso(total['total']),
        )
    if pessoa == "F":
        context['beneficiarias'] = representante

    if pessoa == "J":
        context['beneficiarias'] = beneficiarias

    if request.GET.getlist('gerar'):
        if pessoa == "F":
            context['beneficiarias'] = representante
            creat_auth(requisicao,representante=representante)
            for item in itens:
                a = autorizacao.get(fk_representante=item.item.fk_representante)
                AutorizacaoReqItens.objects.get_or_create(fk_req_item=item,fk_autorizacao=a)
        if pessoa == "J":
            creat_auth(requisicao,beneficiarias=beneficiarias)
            for item in itens:
                a = autorizacao.get(fk_beneficiaria=item.item.fk_beneficiaria)
                AutorizacaoReqItens.objects.get_or_create(fk_req_item=item,fk_autorizacao=a)
        return HttpResponseRedirect('/requisicao/{}/{}/'.format(pessoa,pk))
    
    if request.GET.getlist('DownloadLib'):
        return ReportGenerator().liberacao(requisicao,context,itens)
    return render(request,template_name,context)


def auth_view(request,pk):
    template_name = 'autorizacao/view.html'
    autorizacao = get_object_or_404(Autorizacao, pk=pk)
    requisicao = get_object_or_404(Requisicao, pk=autorizacao.fk_requisicao_id)
    query = AutorizacaoReqItens.objects.filter(fk_autorizacao=autorizacao)
    eds = EDespesa.objects.filter(
        id__in = query.values_list('fk_req_item__ed', flat=False)
        )
    itens = ReqItem.objects.filter(
        item_id__in = query.values_list('fk_req_item__item__id', flat=False),fk_requisicao=requisicao
        )
    total = total_f(itens,i='id',q='quantidade',v='item__valor_unit')
    itemed = item_ed(eds,itens,total)
    pregao = requisicao.fk_pregao.n_pregao
    if len(requisicao.fk_pregao.n_pregao)<3:
        pregao = mask(str(requisicao.fk_pregao.n_pregao),'pregao')
    context = dict(
                rows = autorizacao,
                ano = requisicao.get_ano_display(),
                requisicao = requisicao.requisicao,
                fk_pregao = "{}/{}".format(pregao,requisicao.fk_pregao.get_ano_display()),
                secretaria = requisicao.secretaria,
                n_proc = requisicao.n_proc,
                objeto = str(requisicao.objeto).upper(),
                pa = requisicao.pa,
                eds = [mask(str(ed.ed),'ed') for ed in eds],
                meta = requisicao.meta,
                acao = requisicao.acao, 
                fonte = mask(str(requisicao.fonte),'fr'),
                justificativa = requisicao.justificativa,
                empenho = requisicao.empenho,
                itemed = itemed,
                total = total['total'],
                extenso = total['extenso'],
                assinatura = formats.date_format(requisicao.assinatura, "SHORT_DATE_FORMAT"),
                encerramento = formats.date_format(requisicao.vigencia, "SHORT_DATE_FORMAT"),
                dom_pregao = requisicao.fk_pregao.dom,
                data_pregao_dom = formats.date_format(requisicao.fk_pregao.data, "SHORT_DATE_FORMAT"),
                vigencia = ((requisicao.vigencia - requisicao.assinatura)//30),
                ass_extenso = requisicao.assinatura.strftime("%d de %B de %Y"),
                )

    context['ocorrencia'] = ["{ocorrencia.ocorrencia}: Retificado pelo Pregão Presencial Nº {pregao} do DOM {dom_pregao} de {data_pregao_dom} conforme Consta no DOM {ocorrencia.dom} de {ocorrencia_data}".format(
            ocorrencia = ocorrencia,
            pregao = context['fk_pregao'],
            dom_pregao = context['dom_pregao'],
            data_pregao_dom = context['data_pregao_dom'],
            ocorrencia_data = formats.date_format(ocorrencia.data, "SHORT_DATE_FORMAT")
        )
        for ocorrencia in requisicao.fk_pregao.ocorrencias.all()]

    if requisicao.pessoa == "J":
        beneficiaria = Beneficiaria.objects.get(
            CNPJ__in =query.values_list('fk_req_item__item__fk_beneficiaria', flat=True)
            )
        context['CNPJ'] = mask(str(beneficiaria.CNPJ),'cnpj')
        context['inscricao'] = mask(str(beneficiaria.inscricao),'inscricao')
        context['fone'] = mask(str(beneficiaria.phone),'fone')
        context['estado'] = beneficiaria.endereco.get_estado_display()
        context['beneficiaria'] = beneficiaria
        context['endereco'] = beneficiaria.endereco
        context['cep'] = mask(str(beneficiaria.endereco.cep),'cep')
        context['beneficiaria'] = beneficiaria

    if requisicao.pessoa == "F":
        representante = Representante.objects.get(
            cpf__in =query.values_list('fk_req_item__item__fk_representante', flat=True)
            )
        context['representante'] = representante
        context['cpf'] = mask(str(representante.cpf),'cpf')
        context['endereco'] = representante.endereco
        context['cep'] = mask(str(representante.endereco.cep),'cep')
        context['representante'] = representante
    if request.GET.getlist('download'):
        return ReportGenerator().autorizacao(context,autorizacao)

    print(context['vigencia'])

    if request.GET.getlist('contrato'):
        return ReportGenerator().contrato(context,autorizacao)
    return render(request,template_name,context)


def csv_add(request):
    template_name = "itens/add.html"
    return render(request, template_name, locals())



""" Métodos Total/Máscaras/ItemDespesas """

def creat_auth(instance,beneficiarias=None,representante=None):
    if beneficiarias:
        for b in beneficiarias:
            Autorizacao.objects.get_or_create(autorizacao=instance.aut,contrato=1,fk_requisicao=instance,fk_representante=None,fk_beneficiaria=b)
    else:
        for r in representante:
            Autorizacao.objects.get_or_create(autorizacao=instance.aut,contrato=1,fk_requisicao=instance,fk_representante=r,fk_beneficiaria=None)


def item_ed(eds,itens,total):
    defdic = defaultdict(list)
    itemed = dict()
    for ed in eds:
        for item,subtotal,valor_unit in zip(itens,total['subtotal'],total['valor_unit']):
            if ed == item.ed:
                defdic[mask(str(ed.ed),'ed'),].append([item,subtotal,valor_unit])

    for k, v in defdic.items():
        itemed[k]=v
    return itemed.items()


def total_f(instance,**v):
    grand = 0
    t = instance.values(v['q'],v['v'])
    subtotal = []
    valor_unit = []
    list_total = []
    for x in t:
        subtotal.append(locale.format('%.2f', (x['quantidade']*x['item__valor_unit']), True))
        valor_unit.append(locale.format('%.2f', x['item__valor_unit'], True))
        """locale.currency(1234567, grouping=True)"""
        list_total.append(x['quantidade']*x['item__valor_unit'])
    total = locale.format('%.2f', sum(list_total), True)
    extenso = converter_extenso(sum(list_total))

    return {'subtotal':subtotal,'valor_unit':valor_unit,'total':total, 'extenso':extenso}

def mask(value,types):
    masks = dict(
                ed      = list(reversed("0.0.00.00.00")),
                fr      = list(reversed("000.000.000")),
                cep      = list(reversed("00.000-000")),
                cpf      = list(reversed("000.000.000-00")),
                cnpj    = list(reversed("00.000.000/0000-00")),
                inscricao    = list(reversed("00.000.000-0")),
                fone    = list(reversed("90000 - 0000")),
                pregao    = list(reversed("000")),
            )

    c = len(value)-1
    mask = masks[types]
    for n,x in enumerate(mask):
        if x == '0' and c>=0:
            mask[n]=value[c]
            c-=1
    return ''.join(reversed(mask))