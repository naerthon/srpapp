from django import forms
from .models import Requisicao, ReqItem
from django.forms.extras.widgets    import SelectDateWidget
from django.forms.formsets import formset_factory, BaseFormSet


class RequisicaoForm(forms.ModelForm):
    class Meta:
        model = Requisicao
        fields = [
            'fk_pregao','secretaria','ano','n_proc','data_pro',
            'assinatura','requisicao','ed','pa','meta','acao',
            'fonte','aut','justificativa','contrato','empenho','vigencia','objeto',
        ]
        widgets = {
            'fk_pregao' : forms.Select(attrs={
                'placeholder':'Pregão',
                'class':'form-control',
            }),
            'secretaria' : forms.Select(attrs={
                'placeholder':'Secretaria',
                'class':'form-control',
            }),
            'ano' : forms.Select(attrs={
                'placeholder':'Ano',
                'class':'form-control',
            }),
            'n_proc' : forms.NumberInput(attrs={
                'placeholder':'Nº do Processo',
                'class':'form-control',
            }),
            'data_pro' : forms.DateInput(attrs={
                'placeholder':'Data do Processo',
                'class':'form-control',
            }),
            'vigencia' : forms.DateInput(attrs={
                'placeholder':'Data de Encerramento',
                'class':'form-control',
            }),
            'assinatura' : forms.DateInput(attrs={
                'placeholder':'Assinatura',
                'class':'form-control',
            }),
            'requisicao' : forms.NumberInput(attrs={
                'placeholder':'Nº da Requisicao',
                'class':'form-control',
            }),
            'pa' : forms.NumberInput(attrs={
                'placeholder':'Projeto/Atividade',
                'class':'form-control',
            }),
            'ed' : forms.TextInput(attrs={
                'placeholder':'Elemento de Despesa',
                'class':'form-control',
                'data-inputmask':"'mask': '9.9.99.99.99'",
            }),
            'meta' : forms.NumberInput(attrs={
                'placeholder':'Meta',
                'class':'form-control',
            }),
            'acao' : forms.NumberInput(attrs={
                'placeholder':'Ação',
                'class':'form-control',
            }),
            'fonte' : forms.NumberInput(attrs={
                'placeholder':'Fonte',
                'class':'form-control',
            }),
            'aut' : forms.NumberInput(attrs={
                'placeholder':'Autorização',
                'class':'form-control',
            }),
            'justificativa' : forms.Textarea(attrs={
                'placeholder':'Justificativa',
                'class':'form-control',
                'cols': 5, 'rows': 5
            }),
            'objeto' : forms.TextInput(attrs={
                'placeholder':'Objeto',
                'class':'form-control',
            }),
            'contrato' : forms.CheckboxInput(attrs={'class': 'extclasses'}),
            'empenho' : forms.Select(attrs={
                'placeholder':'Tipo de Empenho',
                'class':'form-control',
            }),

        }


class ReqItemForm(forms.ModelForm):

    class Meta:
        model = ReqItem
        fields = ["id","item", "fk_requisicao","ordem", "quantidade","ed"]
        widgets = {
            'ordem' : forms.HiddenInput(),
            'item' : forms.Select(attrs={
                'placeholder':'Item',
                'class':'form-control',
            }),
            'quantidade' : forms.TextInput(attrs={
                'placeholder':'Quantidade',
                'class':'form-control',
            }),
            'ed' : forms.Select(attrs={
                'class':'form-control',
            }),
            'fk_requisicao' : forms.HiddenInput(),
        }


class CsvForm(forms.Form):
    options = (
                ('1','Sim'),
                ('2','Não'),
                )
    file  = forms.FileField()
    ordenar = forms.ChoiceField(choices=options)