from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from .views import (index,requisicao_view,requisicao_add,auth_view,csv_add,
					beneficiarias, secretarias, beneficiaria_view)

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^beneficiarias$', beneficiarias, name='beneficiarias'),
    url(r'^secretarias$', secretarias, name='secretarias'),
    url(r'^beneficiarias/view/(?P<pk>[0-9]+)/$', beneficiaria_view, name='beneficiaria-view'),
    url(r'^requisicao/add/$', requisicao_add, name='requisicao-add'),
    url(r'^requisicao/(?P<pessoa>[F|J]+)/(?P<pk>[0-9]+)/$', requisicao_view, name='requisicao-view'),
    url(r'^autorizacao/(?P<pk>[0-9]+)/$', auth_view, name='auth-view'),
    url(r'^itens/add/$', csv_add, name='csv-add'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)