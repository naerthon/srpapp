# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-10-16 11:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_auto_20171016_0939'),
    ]

    operations = [
        migrations.AddField(
            model_name='endereco',
            name='complemento',
            field=models.CharField(default=1, max_length=100, verbose_name='Complemento'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='endereco',
            name='numero',
            field=models.IntegerField(verbose_name='Nº'),
        ),
    ]
