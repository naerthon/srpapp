from django.contrib import admin
from django.db 		import models
from .models import Endereco,Beneficiaria,Representante,Ocorrencias,Pregao,Secretaria,Item,EDespesa,Requisicao,ReqItem,Autorizacao,AutorizacaoReqItens


class EnderecoAdmin(admin.ModelAdmin):
    fields = ('logradouro','complemento','bairro','numero','estado','cidade','cep',)
admin.site.register(Endereco,EnderecoAdmin)

class BeneficiariaAdmin(admin.ModelAdmin):
    fields = ('beneficiaria','CNPJ','inscricao','endereco','phone','fk_representante','email',)
    list_display = ('CNPJ','beneficiaria',)
    list_filter = ('CNPJ','beneficiaria',)
    search_fields = ('beneficiaria','CNPJ',)
admin.site.register(Beneficiaria,BeneficiariaAdmin)

class RepresentanteAdmin(admin.ModelAdmin):
    fields = ('representante','cpf','rg','orgao','phone','endereco')
admin.site.register(Representante,RepresentanteAdmin)

class PregaoAdmin(admin.ModelAdmin):
    fields = ('n_pregao','ano','objeto','n_proc','ata','dom','data','ocorrencias','file',)
admin.site.register(Pregao,PregaoAdmin)

class OcorrenciasAdmin(admin.ModelAdmin):
    fields = ('ocorrencia','dom','data',)
admin.site.register(Ocorrencias,OcorrenciasAdmin)

class SecretariaAdmin(admin.ModelAdmin):
    fields = ('secretaria','sigla','representante',)
admin.site.register(Secretaria,SecretariaAdmin)

class ItemAdmin(admin.ModelAdmin):
    fields = ('id_item','desc','tipo','valor_unit','qtd_licit','pessoa','fk_representante','fk_beneficiaria','fk_pregao',)
admin.site.register(Item,ItemAdmin)

class EDespesaAdmin(admin.ModelAdmin):
    fields = ('ed',)
admin.site.register(EDespesa,EDespesaAdmin)

class RequisicaoAdmin(admin.ModelAdmin):
    fields = ('fk_pregao','secretaria','ano','n_proc','data_pro','assinatura','vigencia','objeto',
        'requisicao','ed','pa','meta','acao','fonte','aut','justificativa','empenho','contrato','pessoa',)
admin.site.register(Requisicao,RequisicaoAdmin)

class ReqItemAdmin(admin.ModelAdmin):
    fields = ('fk_requisicao','item','ordem','quantidade','ed',)
admin.site.register(ReqItem,ReqItemAdmin)

class AutorizacaoAdmin(admin.ModelAdmin):
    fields = ('autorizacao','contrato','fk_requisicao','fk_beneficiaria','fk_representante')
admin.site.register(Autorizacao,AutorizacaoAdmin)

class AutorizacaoReqItensAdmin(admin.ModelAdmin):
    fields = ('fk_autorizacao','fk_req_item')
admin.site.register(AutorizacaoReqItens,AutorizacaoReqItensAdmin)