ext = [{1:"Um", 2:"Dois", 3:"Três", 4:"Quatro", 5:"Cinco", 6:"Seis", 7:"Sete", 8:"Oito", 9:"Nove", 10:"Dez", 11:"Onze", 12:"Doze",13:"Treze", 14:"Quatorze", 15:"Quinze", 
16:"Dezesseis", 17:"Dezessete", 18:"Dezoito", 19:"Dezenove"}, 
{2:"Vinte", 3:"Trinta", 4:"Quarenta", 5:"Cinquenta", 6:"Sessenta", 7:"Setenta", 8:"Oitenta", 9:"Noventa"}, 
{1:"Cento", 2:"Duzentos", 3:"Trezentos", 4:"Quatrocentos", 5:"Quinhentos", 6:"Seiscentos", 7:"Setecentos", 8:"Oitocentos", 9:"Novecentos"}]

und = ['', ' mil', (' milhão', ' milhões'), (' bilhão', ' bilhões'), (' trilhão', ' trilhões')]

def cent(s, grand):
    s = '0' * (3 - len(s)) + s
    if s == '000':
        return ''
    if s == '100': 
        return 'cem'
    ret = ''
    dez = s[1] + s[2]
    if s[0] != '0':
        ret += ext[2][int(s[0])]
        if dez != '00':
            ret += ' e '
        else:
            return ret + (type(und[grand]) == type(()) and (int(s) > 1 and und[grand][1] or und[grand][0]) or und[grand])
    if int(dez) < 20:
        ret += ext[0][int(dez)]
    else:
        if s[1] != '0':
            ret += ext[1][int(s[1])]
            if s[2] != '0':
                ret += ' e ' + ext[0][int(s[2])]
    
    return ret + (type(und[grand]) == type(()) and (int(s) > 1 and und[grand][1] or und[grand][0]) or und[grand])

def extenso(reais,centavos):
    ret = []
    grand = 0
    if (int(centavos)==1):
        ret.append('um centavo')
    elif (int(centavos)==0):
        ret.append(cent(centavos,0)+'')
    else:
        ret.append(cent(centavos,0)+' centavos')
    if (int(reais)==0):
        ret.append('')
        ret.reverse()
        return ' e '.join([r for r in ret if r])
    elif (int(reais)==1):
        ret.append('um real')
        ret.reverse()
        return ' e '.join([r for r in ret if r])
    while reais:
        s = reais[-3:]
        reais = reais[:-3]
        if (grand == 0):
            ret.append(cent(s, grand)+' reais')
        else:
            ret.append(cent(s, grand))
        grand += 1
    ret.reverse()
    if ret == ['um milhão', '', ' reais', '']:
        return "Um Milhão de Reais"
    elif ret == ['dez milhões', '', ' reais', '']:
        return "Dez Milhões de Reais"
    else:
        return ' e '.join([r for r in ret if r])

def converter_extenso(valor):
    n = str(valor)
    try:
        reais,centavos = n.split('.')
    except:
        return 'Erro ao parsear o numero informado!'
    if (len(centavos)==1):
        centavos+='0'
    return extenso(reais,centavos)