from secretary import Renderer
from django.http import HttpResponse
import os, tempfile


class ReportGenerator():
    """ Class ReportGenerator """

    @staticmethod
    def autorizacao(data, autorizacao):
        engine = Renderer()
        root = os.path.dirname(__file__)
        document = root + '/templates/relatorio/autorizacao.odt'
        result = engine.render(document, data=data,autorizacao=autorizacao,)
        response = HttpResponse(content_type='application/vnd.oasis.opendocument.text; charset=UTF-8')
        response['Content-Disposition'] = 'inline; filename=Autorização - {}.odt'.format(autorizacao.autorizacao)
        with tempfile.TemporaryFile() as f:
            f.write(result)
            f.seek(0)
            response.write(f.read())
        return response
    
    @staticmethod
    def liberacao(requisicao,data,itens):
        engine = Renderer()
        root = os.path.dirname(__file__)
        document = root + '/templates/relatorio/liberacao.odt'
        result = engine.render(document, requisicao=requisicao,data=data,itens=itens)
        response = HttpResponse(content_type='application/vnd.oasis.opendocument.text; charset=UTF-8')
        response['Content-Disposition'] = 'inline; filename=Liberação - {}.odt'.format(requisicao.aut)
        with tempfile.TemporaryFile() as f:
            f.write(result)
            f.seek(0)
            response.write(f.read())
        return response

    @staticmethod
    def contrato(data, autorizacao):
        engine = Renderer()
        root = os.path.dirname(__file__)
        document = root + '/templates/relatorio/contrato.odt'
        result = engine.render(document, data=data,autorizacao=autorizacao,)
        response = HttpResponse(content_type='application/vnd.oasis.opendocument.text; charset=UTF-8')
        response['Content-Disposition'] = 'inline; filename=Contrato Administrativo - {}/{} ({}).odt'.format(autorizacao.contrato,data['ano'],data['secretaria'].sigla)
        with tempfile.TemporaryFile() as f:
            f.write(result)
            f.seek(0)
            response.write(f.read())
        return response