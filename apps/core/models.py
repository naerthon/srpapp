from django.db import models
from django.core.validators import MaxValueValidator,MinValueValidator,MinLengthValidator
from datetime import datetime
from django.utils               import timezone
from django.db.models.signals   import pre_save,post_save
from django.core.exceptions import ValidationError
from django.conf import settings
import csv
ANOS=[]
for n,ano in enumerate(range(2017, datetime.now().year+1)):
      ANOS.append((str(n),str(ano)))

ESTADOS=[
            ('1','AC'),('2','AL'),('3','AP'),('4','AM'),
            ('5','BA'),('6','CE'),('7','DF'),('8','ES'),
            ('9','GO'),('10','MA'),('11','MT'),('12','MS'),
            ('13','MG'),('14','PA'),('15','PB'),('16','PR'),
            ('17','PE'),('18','PI'),('19','RJ'),('20','RN'),
            ('21','RS'),('22','RO'),('23','RR'),('24','SC'),
            ('25','SP'),('26','SE'),('27','TO'),
      ]

PESSOA = [['J','Jurídica'],['F','Física']]

EMPENHO = [
          ['Estimativo','Estimativo'],
          ['Global','Global'],
          ['Ordinário','Ordinário']
]

class Secretaria(models.Model):
      secretaria = models.CharField(max_length=255,verbose_name='Secretaria')
      sigla = models.CharField(max_length=255,verbose_name='Sigla')
      representante = models.CharField(max_length=255,verbose_name='Secretário(a)')
      
      class Meta:
            ordering = ["secretaria"]
            verbose_name = "Secretaria"
            verbose_name_plural = "Secretarias"

      def __str__(self):
            return self.secretaria

class Endereco(models.Model):
      logradouro = models.CharField(max_length=100, verbose_name='Logradouro')
      complemento = models.CharField(max_length=100, blank=True,null=True, verbose_name='Complemento')
      bairro = models.CharField(max_length=50, verbose_name='Bairro')
      numero = models.IntegerField(verbose_name='Nº')
      cep = models.IntegerField(validators=[MaxValueValidator(99999999)], verbose_name='CEP')
      cidade = models.CharField(max_length=255, verbose_name='Cidade')
      estado = models.CharField(max_length=255, choices=ESTADOS, default=18,verbose_name='Estado')

      class Meta:
            verbose_name = "Endereço"
            verbose_name_plural = "Endereços"

      def __str__(self):
            return "{}, Bairro - {}, Nº {}".format(self.logradouro,self.bairro,self.numero,)

class Representante(models.Model):
      cpf = models.IntegerField(primary_key=True,unique=True,verbose_name='CPF')
      rg = models.IntegerField(verbose_name='RG')
      orgao = models.IntegerField(verbose_name='Orgão Emissor')
      representante = models.CharField(max_length=255,verbose_name='Representante')
      endereco = models.ForeignKey(Endereco, verbose_name="Endereço")
      phone = models.IntegerField(validators=[MaxValueValidator(99999999999)],verbose_name='Telefone')

      class Meta:
            verbose_name = "Representante"
            verbose_name_plural = "Representantes"

      def __str__(self):
            return self.representante

class Beneficiaria(models.Model):
      CNPJ = models.IntegerField(primary_key=True,unique=True,verbose_name='CNPJ')
      beneficiaria = models.CharField(max_length=255,verbose_name='Beneficiário(a)')
      inscricao = models.IntegerField(validators=[MaxValueValidator(999999999)],verbose_name='Inscrição Estadual')
      endereco = models.ForeignKey(Endereco, verbose_name="Endereço")
      email = models.EmailField(max_length=255,verbose_name='email')
      phone = models.IntegerField(validators=[MaxValueValidator(999999999)],verbose_name='phone')
      fk_representante = models.ManyToManyField(Representante, verbose_name='Representante')
      
      class Meta:
            verbose_name = "Beneficiária"
            verbose_name_plural = "Beneficiárias"

      def __str__(self):
            return self.beneficiaria

class Ocorrencias(models.Model):
      OPCOES = [
            ['RETIFICAÇÃO','RETIFICAÇÃO'],
            ['REALINHAMENTO','REALINHAMENTO'],
      ]
      ocorrencia = models.CharField(max_length=50,verbose_name='Ocorrência', choices=OPCOES)
      dom = models.CharField(max_length=50,verbose_name='DOM')
      data = models.DateField(verbose_name='Data da Publicação',default=timezone.now)

      class Meta:
            verbose_name = "Ocorrência"
            verbose_name_plural = "Ocorrências"

      def __str__(self):
            return "{}.{}.{}".format(self.ocorrencia,self.dom,self.data)


class Pregao(models.Model):
      n_pregao = models.CharField(max_length=255,verbose_name='Nº do Pregão')
      ano = models.CharField(max_length=255,verbose_name='Ano', choices=ANOS, default=len(ANOS))
      objeto = models.TextField(max_length=255,verbose_name='Objeto')
      n_proc = models.CharField(max_length=255,verbose_name='Processo Administrativo')
      ata = models.CharField(max_length=255,verbose_name='Ata de Extrato Parcial')
      ocorrencias = models.ManyToManyField(Ocorrencias,verbose_name='Ocorrências')
      dom = models.CharField(max_length=50,verbose_name='DOM')
      data = models.DateField(verbose_name='Data da Publicação',default=timezone.now)
      file = models.FileField(blank=True,null=True)

      class Meta:
            unique_together = ('n_pregao', 'ano')
            verbose_name = "Pregão"
            verbose_name_plural = "Pregões"

      def __str__(self):
            return "{}/{}".format(self.n_pregao,self.get_ano_display())

def add_item(sender, instance, raw=False, **kwargs):
      file = settings.MEDIA_ROOT+'{}'.format(instance.file)
      try:
            with open(file, 'r') as ifile:
                  reader = csv.reader(ifile)
                  for row in reader:
                        ben,pessoa,item,desc,tipo,qnt,valor="".join(row).split(";")
                        if pessoa == 'J':
                              beneficiaria=Beneficiaria.objects.get(CNPJ=ben)
                              Item.objects.get_or_create(id_item=item, desc=desc, tipo=tipo, valor_unit=valor, qtd_licit=qnt, fk_pregao=instance, pessoa=pessoa, fk_beneficiaria=beneficiaria, fk_representante=None)
                        if pessoa == 'F':
                              representante=Representante.objects.get(cpf=int(ben))
                              Item.objects.get_or_create(id_item=item, desc=desc, tipo=tipo, valor_unit=valor, qtd_licit=qnt, fk_pregao=instance, pessoa=pessoa, fk_beneficiaria=None, fk_representante=representante)
      except Exception as e:
            pass
post_save.connect(add_item, sender=Pregao)

class Item(models.Model):
      id_item = models.IntegerField(blank=True,null=True, verbose_name='Item')
      desc = models.TextField(max_length=255,blank=True,null=True, verbose_name='Descrição')
      pessoa = models.CharField(max_length=255,blank=True,null=True, verbose_name='Pessoa', choices=PESSOA, default='J')
      tipo = models.CharField(max_length=255,blank=True,null=True, verbose_name='Tipo')
      qtd_licit = models.DecimalField(max_digits=10, decimal_places=2,blank=True,null=True, verbose_name='QNT Licitada')
      valor_unit = models.DecimalField(max_digits=10, decimal_places=2,blank=True,null=True, verbose_name='Valor unitário')
      fk_pregao = models.ForeignKey(Pregao, blank=True,null=True, verbose_name='Pregão referente')
      fk_beneficiaria = models.ForeignKey(Beneficiaria,blank=True,null=True, verbose_name='Beneficiária')
      fk_representante = models.ForeignKey(Representante,blank=True,null=True, verbose_name='Representante')

      def clean(self):
            super(Item, self).clean()
            if self.pessoa=='F':
                  self.fk_beneficiaria = None
            if self.pessoa=='J':
                  self.fk_representante = None


      class Meta:
            unique_together = ('id_item', 'fk_pregao')
            verbose_name = "Item"
            verbose_name_plural = "Itens"

      def __str__(self):
            return "{} - {}".format(self.id_item,self.fk_pregao)

class EDespesa(models.Model):
      ed = models.IntegerField(validators=[MinValueValidator(10000000, message="Certifique-se que este valor possui 8 digitos."),MaxValueValidator(99999999)],verbose_name='Elemento de Despesa')

      class Meta:
        verbose_name = "Elemento Despesa"
        verbose_name_plural = "Elementos Despesas"

      def __str__(self):
        return "{}".format(self.ed)


class Requisicao(models.Model):
      fk_pregao = models.ForeignKey(Pregao)
      secretaria = models.ForeignKey(Secretaria)
      ano = models.CharField(max_length=255,verbose_name='Ano', choices=ANOS, default=len(ANOS))
      n_proc = models.CharField(max_length=255,verbose_name='Processo Administrativo')
      data_pro = models.DateField(verbose_name='Data Processo',default=timezone.now)
      assinatura = models.DateField(verbose_name='Data de Assinatura',default=timezone.now)
      vigencia = models.DateField(verbose_name='Data de Encerramento',default=timezone.now)
      requisicao = models.CharField(max_length=255,verbose_name='Nº Requisição')
      objeto = models.CharField(max_length=255,verbose_name='Descrição do Objeto')
      ed = models.CharField(max_length=255,verbose_name='Elemento de Despesa')
      pa = models.IntegerField(validators=[MinValueValidator(1000, message="Certifique-se que este valor possui 4 digitos."),MaxValueValidator(9999)],verbose_name='Projeto/Atividade')
      meta = models.CharField(max_length=255,verbose_name='Meta')
      acao = models.IntegerField(verbose_name='Ação')
      fonte = models.IntegerField(validators=[MinValueValidator(1000000, message="Certifique-se que este valor possui 9 digitos."),MaxValueValidator(999999999)],verbose_name='Fonte')
      aut = models.IntegerField(verbose_name='Autorização')
      justificativa = models.TextField(verbose_name='Justificativa')
      empenho = models.CharField(max_length=20,verbose_name='Empenho', choices=EMPENHO, default='Global')
      contrato = models.BooleanField(default=True)
      pessoa = models.CharField(max_length=2,verbose_name='Pessoa', choices=PESSOA, default='J')

      def clean(self):
            super(Requisicao, self).clean()
            ed = self.ed.split(',')
            error = [x for x in ed if len(x)!=8]
            if len(ed)!=len(list(set(ed))):
                  raise ValidationError("Elementos de Despesa Duplicados.")
            elif error:
                  raise ValidationError("Certifique-se de que cada Elemento de Despesa possui 8 digitos.")

      class Meta:
            unique_together = ('n_proc','ano')
            verbose_name = "Requisição"
            verbose_name_plural = "Requisições"

      def __str__(self):
            return "Processo: {}/{}".format(self.n_proc,self.get_ano_display())

def add_ed(sender, instance, raw=False, **kwargs):
    e_despesa = instance.ed.split(',')
    for ed in e_despesa:
        EDespesa.objects.get_or_create(ed=ed)
pre_save.connect(add_ed, sender=Requisicao)

class ReqItem(models.Model):
      fk_requisicao = models.ForeignKey(Requisicao)
      item = models.ForeignKey(Item)
      ordem = models.IntegerField(verbose_name="Ordem")
      quantidade = models.IntegerField(verbose_name="Quantidade")
      ed = models.ForeignKey(EDespesa)

      class Meta:
        unique_together = ('fk_requisicao', 'item')
        verbose_name = "Item/Requisição"
        verbose_name_plural = "Item/Requisições"

      def __str__(self):
        return "{} - {} {}".format(self.ordem,self.item,self.fk_requisicao)

class Autorizacao(models.Model):
      autorizacao = models.CharField(max_length=255,verbose_name='Nº Autorização',blank=True,null=True,)
      contrato = models.IntegerField(blank=True,null=True,verbose_name='Nº Contrato')
      fk_requisicao = models.ForeignKey(Requisicao, verbose_name='Requisição')
      fk_beneficiaria = models.ForeignKey(Beneficiaria,blank=True,null=True, verbose_name='Beneficiária')
      fk_representante = models.ForeignKey(Representante,blank=True,null=True, verbose_name='Representante')

      class Meta:
        verbose_name = "Autorização"
        verbose_name_plural = "Autorizações"


      def __str__(self):
            return "{}".format(self.autorizacao)

def add_aut(sender, instance, raw=False, **kwargs):
      count = Autorizacao.objects.filter(fk_requisicao=instance.fk_requisicao).count()
      if not instance.id:
            if count:
                  instance.autorizacao = "{}.{}".format(instance.autorizacao, count)
pre_save.connect(add_aut, sender=Autorizacao)

class AutorizacaoReqItens(models.Model):
      fk_autorizacao = models.ForeignKey(Autorizacao)
      fk_req_item = models.ForeignKey(ReqItem)

      class Meta:
            verbose_name = "Autorização/item"
            verbose_name_plural = "Autorizações/item"

      def __str__(self):
            return "{} - {}".format(self.fk_req_item,self.fk_autorizacao)