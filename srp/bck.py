def requisicao_view(request,pk):
    template_name = 'requisicao/index.html'
    rows = Autorizacao.objects.filter(fk_requisicao=pk)
    requisicao = get_object_or_404(Requisicao,pk=pk)
    pregao = requisicao.fk_pregao
    b = Beneficiaria.objects
    beneficiarias = []
    for x in b.filter(item__fk_pregao=pregao):
        beneficiarias.append(x.id)
    print(sorted(set(beneficiarias)))
    form = AutorizacaoForm(request.POST)
    erro = None
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/requisicao/{}'.format(pk))
        else:
            erro = "#myModal"
    else:
        form = AutorizacaoForm(initial={'fk_requisicao': pk, 'contrato':Autorizacao.objects.filter(contrato=True).count()+1})
        form.fields['fk_requisicao'].queryset = requisicao
        form.fields['beneficiaria'].queryset = b.filter(id__in=beneficiarias)
    context = dict(rows=rows,form=form, erro=erro)
    requisicao = get_object_or_404(Requisicao,pk=pk)
    itens = '1,2,3,4,5'.split(',')
    ia = ItemAuth.objects.filter(fk_autorizacao__fk_requisicao=pk,item__id_item__in=itens)
    beneficiaria = []

    for row in ia:
        beneficiaria.append(str(row.item.fk_beneficiaria_id))
        print(row.item.id_item,row.item.fk_beneficiaria,)
    print(requisicao, sorted(set(beneficiaria)))
    
    return render(request,template_name,context)